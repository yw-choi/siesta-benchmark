# siesta-benchmark

## Cases
- [Graphene](./Graphene_Pseudo_Basis/README.md)

## References
- [SIESTA docs](https://docs.siesta-project.org/en/latest)
- [Pseudo Dojo](http://www.pseudo-dojo.org/)
- [Simune Atomistics](https://www.simuneatomistics.com/)
