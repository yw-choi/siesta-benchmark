
# Monolayer Graphene Lattice Parameters

| case              | a (Ang) |
| ----------------- | ------- |
| DZP_default |  2.484  |
| DZP_long    | 2.474 |
| simune | 2.471 |
| simune+diffuse | 2.471 |
