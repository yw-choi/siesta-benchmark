#
SystemLabel        graphene

NumberOfAtoms      2
NumberOfSpecies    1

LatticeConstant 1.000 Ang
%block LatticeVectors
        2.142913   -1.237212    0.000000
        2.142913    1.237212    0.000000
        0.000000    0.000000   25.000000
%endblock LatticeVectors

%block Chemical_Species_label
1  6  C
%endblock Chemical_Species_label

AtomicCoordinatesFormat Fractional
%block AtomicCoordinatesAndAtomicSpecies
0.3333333333 0.3333333333 0.5 1
0.6666666666 0.6666666666 0.5 1
%endblock AtomicCoordinatesAndAtomicSpecies

PAO.BasisSize           DZP
PAO.EnergyShift         10 meV
PAO.SplitNorm           0.4

%block PAO.Basis
C          4
 n=2   0   2
   0.0 0.0
 n=2   1   2 P   1
   0.0 0.0
 n=3   0   1
   10.0
 n=3   1   1
   12.0
%endblock PAO.Basis

XC.functional            GGA
XC.authors               PBE

MeshCutoff               800.0 Ry
OccupationFunction       MP
OccupationMPOrder        1
ElectronicTemperature    300 K

%block kgrid_Monkhorst_Pack
     24      0     0        0.0
      0     24     0        0.0
      0      0     1        0.0
%endblock kgrid_Monkhorst_Pack

SolutionMethod           diagon
Diag.ParallelOverK       T
# Diag.Algorithm           ELPA
# NumberOfEigenStates      1300

DM.UseSaveDM             T
MD.UseSaveXV             T

MD.Steps                 0
MD.TypeOfRun             Broyden
MD.MaxForceTol           0.01 eV/Ang
MD.MaxStressTol          0.1 GPa
MD.VariableCell          T

%block Geometry.Constraints
stress 3 4 5 6
%endblock Geometry.Constraints

MaxSCFIterations         1000
SCF.DM.Converge          T
SCF.DM.Tolerance         1.0d-12
SCF.MustConverge         T
SCF.Mix                  Hamiltonian
SCF.Mixer.Method         Pulay
SCF.Mixer.Variant        original
SCF.Mixer.Weight         0.15
SCF.Mixer.History        10

Write.DM                 T
Write.H                  F

WriteMullikenPop         1
WriteEigenvalues         T
WriteCoorInitial         T
WriteCoorStep            T
WriteForces              T
XML.Write                T

SaveHS                   F
SaveRho                  F
SaveDeltaRho             F
SaveRhoXC                F
SaveElectrostaticPotential F
SaveNeutralAtomPotential   F
SaveTotalPotential         F
SaveIonicCharge            F
SaveBaderCharge            F
SaveTotalCharge            F

##################
# Band Structure #
##################
BandLinesScale ReciprocalLatticeVectors
%block BandLines
1  0.0000000   0.00000000 0.000   \Gamma
40 0.5000000   0.00000000 0.000   M
40 0.66666666  0.33333333 0.000   K
40 0.0000000   0.00000000 0.000   \Gamma
%endblock bandlinek

################
# Wavefunction #
#################
# WriteWaveFunctions              F
# WaveFuncKPointsScale ReciprocalLatticeVectors
# %block WaveFuncKPoints
# 0.000  0.000  0.000  from 1 to 10 # Gamma wavefuncs 1 to 10
# 2.000  0.000  0.000  1 3 5        # X wavefuncs 1,3 and 5
# 1.500  1.500  1.500               # K wavefuncs, all
# %endblock WaveFuncKPoints

########
# PDOS #
########
# %block ProjectedDensityOfStates
# -10.00  0.00  0.050  500  eV
# %endblock ProjectedDensityOfStates
# %block PDOS.kgrid_Monkhorst_Pack
#       3      0     0        0.0
#       0      3     0        0.0
#       0      0     1        0.0
# %endblock PDOS.kgrid_Monkhorst_Pack

# %include grimme.fdf

# Slab.DipoleCorrection vacuum
# %block Geometry.Charge
# plane   0
# delta
# 0.0 0.0 2.5 Ang  # An intersection point, in the plane
# 0.0 0.0 1.0      # The normal vector to the plane
# %endblock Geometry.Charge
