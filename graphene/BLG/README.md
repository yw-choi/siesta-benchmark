
## BLG interlayer distance

### pseudo-dojo / DZP+diffuse
- PBE: 3.539425 Ang
- C09: 3.10395925 Ang
- VV: 3.32175 Ang

### simune
- VV: 3.23 Ang

### simune+diffuse
- VV: 3.32 Ang


