from qwt.io.siesta import read_xv, struct2fdf

struct = read_xv('../../BLG/VV/graphene.XV')
struct.make_supercell([7, 7, 1])
struct2fdf(struct, 'struct.fdf')
